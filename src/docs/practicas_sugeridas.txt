PRÁCTICAS SUGERIDAS

Es recomendable la realización de prácticas en todas las unidades que consistan en el
modelado y resolución de problemas utilizando un lenguaje de programación orientado a
objetos; la entrega al final de cada unidad de un proyecto que refuerce en la aplicación de los
temas vistos en clase y la entrega de un proyecto final que converja en la aplicación de los
conceptos vistos en la materia, el cual debe ser definido al final de la primera unidad.
Algunos problemas propuestos:

1. Resolver problemas implementando las diversas estructuras de datos lineales en
forma estática y en forma dinámica.

2. Implementar una función que recibe una lista de enteros L y un número entero n de
forma que modifique la lista mediante el borrado de todos los elementos de la lista que
tengan este valor.

3. Implementar una función Mezcla2 que tenga como parámetros dos listas de enteros
ordenados de menor a mayor y que devuelva una nueva lista como unión de ambas
con sus elementos ordenados de la misma forma.

4. Resolver lo siguiente: se tienen dos pilas que contienen 12 números enteros; la
primera ordenada ascendentemente del 1 al 12 desde el tope hacia el fondo, y la
segunda ordenada descendentemente del 24 al 13 desde el tope hacia el fondo,
elabore un algoritmo que fusione ambas pilas en una tercera ordenada
descendentemente desde el tope hacia el fondo.

5. Simular la lógica de una pila utilizando dos colas.

6. Simular la lógica de una cola usando dos pilas.

7. Escriba un algoritmo de un programa que lea por teclado una palabra no mayor de 20
caracteres, y la imprima al revés. Use pilas y colas.

8. Dado un arreglo constituido de números enteros y que contiene N elementos siendo N
>= 1, implemente una solución que diga si la suma de la primera mitad de los enteros
del array es igual a la suma de la segunda mitad de los enteros del arreglo.

9. Escribir una función recursiva para calcular la altura de un árbol cualquiera.

10. Escribir una función no recursiva para calcular la altura de un árbol cualquiera.

11. Resolver lo siguiente: Supongamos que tenemos una función valor tal que dado un
valor de tipo char (una letra del alfabeto) devuelve un valor entero asociado a dicho
identificador. Supongamos también la existencia de un árbol de expresión T cuyos
nodos hoja son letras del alfabeto y cuyos nodos interiores son los caracteres *, +, -, /.
Diseñar una función que tome como parámetros un nodo y un árbol binario y devuelva
el resultado entero de la evaluación de la expresión representada.

12. Implementar una función no recursiva para recorrer un árbol binario en inorden.

13. Escribir una función recursiva que encuentre el número de nodos de un árbol binario.

14. Realizar un programa que imprima un grafo.

15. Construir un programa que determine el número de componentes conexas que posee
un grafo cualquiera.

16. Resolver lo siguiente: Un grafo no dirigido se dice de Euler si existe un camino
Euleriano que incluye a todas sus aristas. Construir una función que dado un grafo no
dirigido determine si es de Euler o no lo es.

Importante: Todas las practicas aqui expuestas, son sugeridas, por lo que pueden
ser expuestas en clase o no. Se sugiere ampliamente utilizar al menos dos lenguajes 
programación para realizar todas las practicas suegridas aqui y en clase, por otro lado
se sugiere leer los libros que aparecen en la carpeta books del repositorio, este material
es de gran apoyo para comprender al maximo la materia.